# Novem Build System

**Creation Date:** 1 October 2015

**Updated:** 3 March 2016

**Version:** 0.3

**For:** General Use

## Description
This general build system is a barebones repository to quickly install and setup the common tasks used for design and development.

### Node Packages Included
* bower
* browser-sync
* gulp
* gulp-changed
* gulp-cssnano
* gulp-imagemin
* gulp-less
* gulp-plumber
* gulp-rename
* gulp-sass
* gulp-uglify
* gulp-watch
* imagemin-pngquant
* less-plugin-autoprefix
 
### To Use ###
1. Clone git repository into a directory
2. Run `npm install`
3. Run `bower install`
4. Run `gulp copy` to move jQuery and jQuery-UI elements into deployment directory

### Gulp Tasks
**`copy`** -- Copy is an array of tasks. You can (and should) add/remove 'sub-tasks' as needed.

**`copy:jquery`** -- Copy the minimized jQuery Javascript file to the deployment directory.

**`copy:jquery-ui`** -- Copy the minimized jQuery-UI Javascript file to the deployment directory. Also copies the jQuery UI folder over.

**`less`** -- Compiles the less files in `SRC/less` folder, autoprefixes rulesets, minimizes the CSS, and lastly copies the file to `DEST/css` with the `.min.css` suffix.

**`sass`** -- Compiles the less files in `SRC/sass` folder, minimizes the CSS, and lastly copies the file to `DEST/css` with the `.min.css` suffix.

**`script`** -- Compresses the Javascript, and copies the file to `DEST/js` with the `.min.js` suffix.

**`html`** -- Used for `watch` [for BrowserSync].

**`watch`** -- Watch `less`, `sass`, `script`, and `html`.

**`browser-sync`** -- Setup and launch our BrowserSync.

**`server`** -- Launch BrowserSync and `watch` our files.


### To Do
1. Create a named task to handle image compression.
2. Create a `default` task.
3. Create a `build` task that will build our final project and clean up any unnecessary files.


## Changelog

### Version 0.3
*3 Mar 2016*

* Added `gulp-sass`
* Added new task `sass` to `gulpfile.js`
* Added `sass` task to the `watch` task

### Version 0.2
*3 Feb 2016*

* Changed out `gulp-minify-css` for `gulp-cssnano`
* Added changelog information to README.md file
 
### Version 0.1
*1 Oct 2016*

* Original creation of quick build system


#### Project by ####
[Novem Designs, LLC](http://www.novemwebdesign.com)
