var gulp = require('gulp'),
  watch = require('gulp-watch'),
  plumber = require('gulp-plumber'),
  rename = require('gulp-rename'),
  changed = require('gulp-changed'),
  uglify = require('gulp-uglify'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  less = require('gulp-less'),
  sass = require('gulp-sass'),
  autoprefix = require('less-plugin-autoprefix'),
  cssnano = require('gulp-cssnano'),
  browserSync = require('browser-sync').create();

// For browser-sync
var reload = browserSync.reload;

// For less-plugin-autoprefix
var prefix = new autoprefix({ browsers: ["last 2 versions"] });

// Our Source and Destination (root) directories
var SRC = './build/';
var DEST = './deploy/';


/*****************************************************
 *  COPY
 *    Build task to copy package components over
 *    to destination folder.
 *****************************************************/
gulp.task('copy', ['copy:jquery', 'copy:jquery-ui']);

// jQuery Copy
gulp.task('copy:jquery', function () {
  'use strict';

  gulp.src('./bower_components/jquery/dist/jquery.min.js')
    .pipe(plumber())
    .pipe(gulp.dest(DEST + 'js'));

});

// jQuery-UI Copy
gulp.task('copy:jquery-ui', function () {
  'use strict';

  var jquiFiles = [
    './bower_components/jquery-ui/jquery-ui.min.js',
    './bower_components/jquery-ui/ui/**/*.*'
  ];

  gulp.src(jquiFiles, { base: './bower_components/jquery-ui/' })
    .pipe(plumber())
    .pipe(gulp.dest(DEST + 'js'));

});


/*****************************************************
 *  LESS and CSS
 *    Compile LESS files with autoprefix
 *    MinifyCSS before moving
 *****************************************************/
gulp.task('less', function () {
  'use strict';
  
  gulp.src(SRC + 'less/**/*.less')
    .pipe(plumber())
    .pipe(less({ plugins: [ prefix ] }))
    .pipe(cssnano())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(DEST + 'css'))
    .pipe(browserSync.stream({match: '**/*.css'}));

});


/*****************************************************
 *  Sass/SCSS and CSS
 *    Compile Sass/SCSS files with autoprefix
 *    MinifyCSS before moving
 *****************************************************/
gulp.task('sass', function () {
  'use strict';
  
  gulp.src(SRC + 'sass/**/*.scss')
    .pipe(plumber())
    .pipe(sass())
    .pipe(cssnano())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(DEST + 'css'))
    .pipe(browserSync.stream({match: '**/*.css'}));

});


/*****************************************************
 *  JAVASCRIPT
 *    Uglify Javascript files
 *    Move to destination directory
 *****************************************************/
gulp.task('script', function () {
  'use strict';
  
  gulp.src(SRC + 'js/**/*.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(DEST + 'js'))
    .pipe(browserSync.stream({match: '**/*.min.js'}));
});

/*****************************************************
 *  HTML
 *    Monitor HTML files for changes
 *    Used mainly for browser-sync
 *****************************************************/
gulp.task('html', function () {
  'use strict';
  
  gulp.src(DEST + '**/*.html')
    .pipe(browserSync.stream({match: '**/*.html'}));
});


/*****************************************************
 *  WATCH
 *    Watch files/directories for changes
 *
 *****************************************************/
gulp.task('watch', function () {
  'use strict';
  
  gulp.watch(SRC + 'less/**/*.less', ['less']);
  gulp.watch(SRC + 'sass/**/*.scss', ['sass']);
  gulp.watch(SRC + 'js/**/*.js', ['script']);
  gulp.watch(DEST + '**/*.html', ['html']);
});


/*****************************************************
 *  BrowserSync
 *    Create a server to sync browsers
 *
 *****************************************************/
gulp.task('browser-sync', function () {
  'use strict';
  
  browserSync.init({
    server: {
      baseDir: DEST,
      browser: ["chrome"]
    }
  });
});


/*****************************************************
 *  SERVER
 *    Start BrowserSync and Watch files
 *
 *****************************************************/
gulp.task('server', ['browser-sync', 'watch']);